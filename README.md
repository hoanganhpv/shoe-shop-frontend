# This is the entry skills testing project of GOLDEN OWL company.

- Executor: Phan Van Hoang Anh
- Project name: Shoe Shop
- Project description:
The project includes several features for displaying shoe products and adding them to the user's cart.
- Project status: Incomplete
- Progress:
 + Backend: 100%
 + Frontend: 50%

# Frontend technologies
###  Programming Language: Javascript
### Framework: ReactJS
### Features:
The project comprises APIs revolving around three main features:

+ Authentication and authorization
+ Product listing
+ Cart management

#### Note: The user interface of the project, due to some unforeseeable reasons, is currently unable to be completed. I sincerely apologize to the esteemed company and to all of you.

# Execution environment
+ NodeJS (Latest version recommended)

# Operational Steps:
Firstly, in order to utilize the project's features, you must ensure that the backend server of the project has been successfully launched.

Note: Since the project is not yet completed, it hasn't been deployed to a server. Therefore, the APIs used in the user interface are being accessed with the HOST set to localhost and on port 8000.

You should configure the HOST and port on the backend server accurately before using the project's user interface.

Once you have the project source, use the command line to navigate to the project folder and run the following commands sequentially:

- npm install
- npm start

If you have configured all the necessary systems, the application will run successfully.
