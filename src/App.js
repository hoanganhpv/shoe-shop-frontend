import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import { Login } from "./components/Login";
import { Register } from "./components/Register";
import ProductList from "./components/ProductList";
import Cart from "./components/Cart";

function App() {
  const [currentForm, setCurrentForm] = useState('login');

  const toggleForm = (formName) => {
    setCurrentForm(formName);
  }

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/login">
            <Login onFormSwitch={toggleForm} />
          </Route>
          <Route path="/register">
            <Register onFormSwitch={toggleForm} />
          </Route>
          <Route path="/product-list">
            <ProductList />
          </Route>
          <Route path="/cart">
            <Cart /> {/* Đường dẫn cho trang giỏ hàng */}
          </Route>
          <Redirect from="/" to="/login" />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
