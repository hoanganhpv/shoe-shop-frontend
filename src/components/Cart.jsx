import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { updateCartItemQuantitySuccess } from "../reducer/cartItemSlice";
import { useEffect } from "react";
import increase from "../assets/plus.png";
import decrease from "../assets/minus.png";

const Cart = () => {
    const dispatch = useDispatch();
    const [cartItems, setCartItems] = useState([]);
    const token = useSelector((state) => state.auth.token);
    const cartItemList = useSelector((state) => state.cartItem.cartItemList);

    useEffect(() => {
        setCartItems(cartItemList);
    }, [cartItemList]);

    const handleQuantityChange = (item, newQuantity) => {
        if (newQuantity === 0) {
            setCartItems(prevCartItems => prevCartItems.filter(cartItem => cartItem.id !== item.id));
        } else if (newQuantity === 1 && item.quantity === 1) {
            setCartItems(prevCartItems => prevCartItems.filter(cartItem => cartItem.id !== item.id));
        } 
            dispatch(updateCartItemQuantitySuccess({ id: item.id, quantity: newQuantity, token: token }));
    };

    const removeItemFromCart = (item) => {
        // Call API to remove item from cart
        fetch(`http://127.0.0.1:8000/api/cart-items/${item.id}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
        })
        .then(response => {
            if (response.ok) {
                // If API call is successful, remove the item from cart in the frontend
                setCartItems(prevCartItems => prevCartItems.filter(cartItem => cartItem.id !== item.id));
            }
        })
        .catch(error => {
            console.error("Error deleting item from cart:", error);
        });
    };

    const shouldDisplayItem = (item) => {
        return item.quantity > 0;
    };

    return (
        <div className="cart">
            <h2>Giỏ hàng</h2>
            <Link to="/product-list" className="cart-link" style={{
                display: "inline-block",
                backgroundColor: "#007bff",
                color: "#fff",
                padding: "10px 20px",
                borderRadius: "5px",
                textDecoration: "none",
                transition: "background-color 0.3s ease-in-out",
                marginLeft: "10px"
            }}>Trở về cửa hàng</Link>
            <div className="cards-container">
                {cartItems && cartItems.length > 0 ? (
                    cartItems.map((item) => (
                        shouldDisplayItem(item) && (
                            <div key={item.id} className="card">
                                <img src={item.product.image} alt={item.product.name} />
                                <h3>{item.product.name}</h3>
                                <p>Price: ${item.product.price}</p>
                                <button onClick={() => handleQuantityChange(item, item.quantity - 1)}
                                style={{ maxWidth: '10%', maxHeight: '5%', verticalAlign: 'middle' }}
                                >
                                        <img style={{ maxWidth: '100%', maxHeight: '100%', verticalAlign: 'middle' }} src={decrease} alt="Decrease" />
                                    </button>
                                <span>{item.quantity}</span>
                                <button onClick={() => handleQuantityChange(item, item.quantity + 1)}
                                style={{ maxWidth: '10%', maxHeight: '5%', verticalAlign: 'middle' }}
                                >
                                        <img style={{ maxWidth: '100%', maxHeight: '100%', verticalAlign: 'middle' }} src={increase} alt="Increase" />
                                    </button>
                                <button style={{ maxWidth: '10%', maxHeight: '2%', verticalAlign: 'middle' }} onClick={() => removeItemFromCart(item)}
                                >Delete</button>
                            </div>
                        )
                    ))
                ) : (
                    <p>Your cart is empty</p>
                )}
            </div>
        </div>
    );
    
};

export default Cart;
