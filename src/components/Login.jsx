import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { loginSuccess } from "../reducer/authSlice";
import { useDispatch } from "react-redux";
import { getAllCartItemSuccess } from "../reducer/cartItemSlice";
import { getAllProductSuccess } from "../reducer/productSlice";

export const Login = (props) => {
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const history = useHistory();
    const dispatch = useDispatch();

    const handleSubmit = async (e) => {
        e.preventDefault();
        
        const response = await fetch('http://127.0.0.1:8000/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, password: pass }),
        });

        if (response.ok) {
            const data = await response.json();
            dispatch(loginSuccess(data.data.access_token));
            fetchProducts(data.data.access_token);
        } else {
            console.log('Đăng nhập thất bại');
        }
    }

    const fetchProducts = async (token) => {
        const productResponse = await fetch('http://127.0.0.1:8000/api/products', {
            headers: {
                Authorization: `Bearer ${token}`
            },
        });

        if (productResponse.ok) {
            const data = await productResponse.json();
            dispatch(getAllProductSuccess(data.data.products));
            fetchCartItemsAndRedirect(token);
        }
    }

    const fetchCartItemsAndRedirect = async (token) => {
        const cartResponse = await fetch('http://127.0.0.1:8000/api/cart-items', {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        if (cartResponse.ok) {
            const cartData = await cartResponse.json();
            dispatch(getAllCartItemSuccess(cartData.data.cart_items));
        }

        history.push("/product-list");
    }

    return (
        <div className="auth-form-container">
            <h2>Login</h2>
            <form className="login-form" onSubmit={handleSubmit}>
                <label htmlFor="email">email</label>
                <input value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="youremail@gmail.com" id="email" name="email" />
                <label htmlFor="password">password</label>
                <input value={pass} onChange={(e) => setPass(e.target.value)} type="password" placeholder="********" id="password" name="password" />
                <button type="submit">Log In</button>
            </form>
            <Link to="/register" className="link-btn">Don't have an account? Register here.</Link>
        </div>
    )
}
