import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux"; // Import useDispatch
import { addToCartSuccess } from "../reducer/cartItemSlice"; // Import the action
import '../components/css/ProductList.css';

const ProductList = () => {
    const [products, setProducts] = useState(null);
    const productList = useSelector((state) => state.product.productList);
    const cartItems = useSelector((state) => state.cartItem.cartItemList);
    const dispatch = useDispatch(); // Initialize useDispatch

    useEffect(() => {
        if(productList) {
            const updatedProducts = productList.map(product => {
                const isInCart = cartItems.some(item => item.product_id === product.id);
                return { ...product, isInCart };
            });
            setProducts(updatedProducts);
        }
    }, [productList, cartItems]);

    // Function to handle adding a product to cart
    const handleAddToCart = (product) => {
        dispatch(addToCartSuccess({ product_id: product.id })); // Dispatch the addToCart action
    };

    return (
        <div className="product-list">
            <h2>Product List</h2>
            <Link to="/cart" className="cart-link" style={{
                display: "inline-block",
                backgroundColor: "#007bff",
                color: "#fff",
                padding: "10px 20px",
                borderRadius: "5px",
                textDecoration: "none",
                transition: "background-color 0.3s ease-in-out",
                marginLeft: "10px"
            }}>Đi tới giỏ hàng</Link>
            <div className="cards-container">
            {products ? (
                products.map(product => (
                    <div key={product.id} className="card">
                        <img src={product.image} alt={product.name} />
                        <h3>{product.name}</h3>
                        <p>{product.description}</p>
                        <p>Price: ${product.price}</p>
                        {product.isInCart ? (
                            <div className="added-to-cart">✔ Added to Cart</div>
                        ) : (
                            <button onClick={() => handleAddToCart(product)}>Add to Cart</button>
                        )}
                    </div>
                ))
            ) : (
                <p>Hiện tại không có sản phẩm nào</p>
            )}
            </div>
        </div>
    );
};

export default ProductList;
