import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { useSelector } from "react-redux";

const initialState = {
    cartItemList: null
}

const updateCartItemQuantity = async (cartItemId, newcartItemQuantity, token) => {
    console.log(token)
    const updateResponse = await fetch(`http://127.0.0.1:8000/api/cart-items/${cartItemId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({ quantity: newcartItemQuantity }),
    })
}

export const addToCartSuccess = createAsyncThunk(
    "cartItem/addToCartSuccess",
    async ({ product_id }, { getState, dispatch }) => {
        const token = getState().auth.token;
        
        const response = await fetch(`http://127.0.0.1:8000/api/cart-items`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify({ product_id,  quantity: 1 }),
        });

        if (response.ok) {
            const addedCartItem = await response.json();
            dispatch(cartItemSlice.actions.addToCartSuccess(addedCartItem)); // Gọi action để cập nhật state
            return addedCartItem;
        }
    }
);

const cartItemSlice = createSlice({
    name: "cartItem",
    initialState,
    reducers: {
      getAllCartItemSuccess: (state, action) => {
        state.cartItemList = action.payload;
      },
      updateCartItemQuantitySuccess: (state, action) => {
        const { id, quantity, token } = action.payload;
        updateCartItemQuantity(id, quantity, token);
        if (state.cartItemList) {
          const updatedCartItems = state.cartItemList.map((item) => {
            console.log(item)  
            return item.id === id ? { ...item, quantity } : item
          }
          );
          
          state.cartItemList = updatedCartItems;
        }
      },
      addToCartSuccess: (state, action) => {
        const addedCartItem = action.payload;
        console.log("Show test: ");
        console.log(action.payload);
        state.cartItemList = [...state.cartItemList, addedCartItem];
    },
    },
  });

export const { getAllCartItemSuccess, updateCartItemQuantitySuccess } = cartItemSlice.actions;

export default cartItemSlice.reducer;