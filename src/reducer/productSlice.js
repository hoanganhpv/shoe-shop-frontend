import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    productList: null
};

const productSlice = createSlice({
    name: "product",
    initialState,
    reducers: {
        getAllProductSuccess: (state, action) => {
            state.productList = action.payload;
        }
    }
});

export const { getAllProductSuccess } = productSlice.actions;

export default productSlice.reducer;