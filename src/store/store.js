import { configureStore } from "@reduxjs/toolkit";
import authSlice from "../reducer/authSlice";
import cartItemSlice from "../reducer/cartItemSlice";
import productSlice from "../reducer/productSlice";

export const store = configureStore({
  reducer: {
    auth: authSlice,
    cartItem: cartItemSlice,
    product: productSlice
  },
});
